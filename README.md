# Socket power for PHP
Socket power for PHP は、PHPでwebsocketを使うためのモジュールです。  

PHPからsocket.ioを直接利用するのではなく、  
websocketのフロントにNodeJSを利用することで、
多数のクライアントから、同時にWeboscketの接続があった場合も、　 
NodeJSの方がPHPより捌ける数が多い気がする！というだけの思いつきで作り始めてます。

## 動作イメージ

### ブロードキャスト
![01](/takyam/socket-power-for-php/raw/09382ad8438a/readme_assets/image01.png)

### websocket request and response
![02](/takyam/socket-power-for-php/raw/09382ad8438a/readme_assets/image02.png)

### イベント登録
![03](/takyam/socket-power-for-php/raw/09382ad8438a/readme_assets/image03.png)